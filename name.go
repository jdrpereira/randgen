package randgen

import (
	"github.com/Pallinder/go-randomdata"
)

// SillyName returns a random silly name.
func SillyName() string {
	return randomdata.SillyName()
}
