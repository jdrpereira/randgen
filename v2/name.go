package randgen

import (
	"github.com/Pallinder/go-randomdata"
)

// SillyName2 returns a random silly name.
func SillyName2() string {
	return randomdata.SillyName()
}
